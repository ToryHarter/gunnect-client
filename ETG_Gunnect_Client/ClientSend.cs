﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETG_Gunnect_Client
{
    class ClientSend
    {
        private static void SendTCPData(Packet packet)
        {
            packet.WriteLength();
            GunnectClient.Instance.Tcp.SendData(packet);
        }

        public static void WelcomeReceived()
        {
            using (Packet packet = new Packet((int)ClientPackets.WelcomeReceived))
            {
                packet.Write(GunnectClient.Instance.Id);
                packet.Write(GunnectClient.Instance.Username);

                SendTCPData(packet);
            }
        }

        public static void DynamicClientMessage(int fromClientId, SendType sendType, List<int> toClients, string modId, string dataTypeName, string data)
        {
            using (Packet packet = new Packet((int)ClientPackets.DynamicClientMessage))
            {
                packet.Write(fromClientId);
                packet.Write((int)sendType);
                packet.Write(JsonConvert.SerializeObject(toClients));
                packet.Write(modId);
                packet.Write(dataTypeName);
                packet.Write(data);

                SendTCPData(packet);
            }
        }

        public static void UpdateClientModDataMessage(int fromClientId, string modId, string serializedModData)
        {
            using (Packet packet = new Packet((int)ClientPackets.UpdateClientModDataMessage))
            {
                packet.Write(fromClientId);
                packet.Write(modId);
                packet.Write(serializedModData);

                SendTCPData(packet);
            }
        }
    }
}
