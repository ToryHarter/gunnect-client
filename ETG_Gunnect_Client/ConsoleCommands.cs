﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETG_Gunnect_Client
{
    class ConsoleCommands
    {
        public static void AddICOCommands()
        {
            ETGModConsole.Commands.AddGroup("gp");
            ETGModConsole.Commands.GetGroup("gp").AddGroup("help", Help);
            ETGModConsole.Commands.GetGroup("gp").AddGroup("connect", Connect);
            ETGModConsole.Commands.GetGroup("gp").AddGroup("reconnect", Reconnect);
            ETGModConsole.Commands.GetGroup("gp").AddGroup("dc", Disconnect);
        }

        // Humbly borrowed from: https://modworkshop.net/mod/23271
        private static void Help(string[] args)
        {
            try
            {
                ETGModConsole.Log("");
                ETGModConsole.Log("<size=100><color=#ff0000ff>Gunnect+</color></size>");
                ETGModConsole.Log("<size=100><color=#ff0000ff>By The_Rot_Bot</color></size>");
                ETGModConsole.Log("<size=100><color=#ff0000ff>--------------------------------</color></size>");
                ETGModConsole.Log("Gunnect+ Commands:");
                ETGModConsole.Log("");
                ETGModConsole.Log("<color=#FFFF33>gp help</color> - lists Gunnect+ commands");
                ETGModConsole.Log("<color=#FFFF33>gp connect [IPv4 Address] [Username]</color> - connects to the player hosted Gunnect+ server at the provided IPv4 address under the given username");
                ETGModConsole.Log("<color=#FFFF33>gp reconnect</color> - reconnects to the previously connected player hosted Gunnect+ server with the then provided username");
                ETGModConsole.Log("<color=#FFFF33>gp dc</color> - disconnect from the current player hosted Gunnect+ server");
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong showing help: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }

        private static void Connect(string[] args)
        {
            try
            {
                if (args.Length >= 1)
                {
                    string username = "";
                    if (args.Length >= 2)
                    {
                        username = args[1];
                    }

                    ETGModConsole.Log("Connecting to server...");
                    if (args[0] == "localhost")
                    {
                        GunnectClient.Instance.ConnectToServer("127.0.0.1", username);
                    }
                    else
                    {
                        GunnectClient.Instance.ConnectToServer(args[0], username);
                    }
                }
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong connecting: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }

        private static void Reconnect(string[] args)
        {
            try
            {
                if (!String.IsNullOrEmpty(GunnectClient.Instance.PreviouslyConnectedIP))
                {
                    Connect(new string[] { GunnectClient.Instance.PreviouslyConnectedIP, GunnectClient.Instance.Username });
                }
                else
                {
                    ETGModConsole.Log("No previous connection to reconnect to!");
                }
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong reconnecting: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }

        private static void Disconnect(string[] args)
        {
            try
            {
                GunnectClient.Instance.Disconnect();
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Something went wrong disconnecting: {e.Message}");
                ETGModConsole.Log(e.InnerException.Message);
            }
        }
    }
}
