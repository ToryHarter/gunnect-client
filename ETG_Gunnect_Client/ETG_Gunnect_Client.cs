﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoMod.RuntimeDetour;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

namespace ETG_Gunnect_Client
{
    public class ETG_Gunnect_Client : ETGModule
    {
        public static List<PlayerInfo> PlayerInfos;

        public static VerticalLayoutGroup PlayerInfoGroup;
        public static RectTransform PlayerInfoGroupRectTransform;
        public static int FontSize;

        public override void Init() { }

        public override void Exit() { }

        public override void Start()
        {
            PlayerInfos = new List<PlayerInfo>();
            FontSize = 42;

            this.SetupTcp();
            this.SetupUI();
            ConsoleCommands.AddICOCommands();
        }

        private void SetupTcp()
        {
            ETGModMainBehaviour.Instance.gameObject.AddComponent<GunnectClient>();
            ETGModMainBehaviour.Instance.gameObject.AddComponent<ThreadManager>();
            ETGModMainBehaviour.Instance.gameObject.AddComponent<GunnectClientHandle>();
        }

        private void SetupUI()
        {
            GUI.Init();

            GameObject groupContainer = new GameObject();
            PlayerInfoGroupRectTransform = groupContainer.AddComponent<RectTransform>();
            PlayerInfoGroupRectTransform.SetTextAnchor(TextAnchor.MiddleLeft);
            GUI.UpdateUIOffset();

            PlayerInfoGroup = groupContainer.AddComponent<VerticalLayoutGroup>();
            PlayerInfoGroup.transform.parent = GUI.m_canvas.transform;
            PlayerInfoGroup.enabled = true;

            var textObject = GUI.CreateText(null, new Vector2(0f, 0f), "Offline", TextAnchor.MiddleLeft, ETG_Gunnect_Client.FontSize);
            textObject.text = "Offline";
            textObject.transform.parent = ETG_Gunnect_Client.PlayerInfoGroup.transform;
        }

        public static void ExternalModInvoke(int sendType, List<int> toClients, string modId, string dataTypeName, string data)
        {
            // This needs to be eventually removed once all mods are using ExternalDynamicClientMessage()
            ExternalDynamicClientMessage(sendType, toClients, modId, dataTypeName, data);
        }

        public static void ExternalDynamicClientMessage(int sendType, List<int> toClients, string modId, string dataTypeName, string data)
        {
            ClientSend.DynamicClientMessage(GunnectClient.Instance.Id, (SendType)sendType, toClients, modId, dataTypeName, data);
        }

        public static void ExternalUpdateClientModDataMessage(string modId, string data, List<string> uiEntries)
        {
            var modData = new ModData
            {
                SerializedData = data,
                UIEntries = uiEntries,
            };
            var serializedModData = JsonConvert.SerializeObject(modData);

            ClientSend.UpdateClientModDataMessage(GunnectClient.Instance.Id, modId, serializedModData);
        }
    }
}
