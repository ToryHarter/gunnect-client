﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ETG_Gunnect_Client
{
	// Humbly borrowed from: https://github.com/An3s079/DPSMod/blob/main/GUI.cs
	public static class GUI
	{
		public static bool Toggle()
		{
			bool flag = !GUI.GUIRoot.gameObject.activeSelf;
			GUI.GUIRoot.gameObject.SetActive(flag);
			return flag;
		}

		public static void SetVisible(bool visible)
		{
			GUI.GUIRoot.gameObject.SetActive(visible);
		}

		public static void Init()
		{
			GUI.GUIController = new GameObject("GUIController").transform;
			UnityEngine.Object.DontDestroyOnLoad(GUI.GUIController.gameObject);
			GUI.CreateCanvas();
			GUI.GUIRoot = GUI.m_canvas.transform;
			GUI.GUIRoot.SetParent(GUI.GUIController);
		}

		public static void CreateCanvas()
		{
			GameObject gameObject = new GameObject("Canvas");
			UnityEngine.Object.DontDestroyOnLoad(gameObject);
			GUI.m_canvas = gameObject.AddComponent<Canvas>();
			GUI.m_canvas.renderMode = RenderMode.ScreenSpaceOverlay;
			GUI.m_canvas.sortingOrder = 100000;
			gameObject.AddComponent<CanvasScaler>();
			gameObject.AddComponent<GraphicRaycaster>();
		}

		public static Text CreateText(Transform parent, Vector2 offset, string text, TextAnchor anchor = TextAnchor.MiddleCenter, int font_size = 20)
		{
			GameObject gameObject = new GameObject("Text");
			gameObject.transform.SetParent((parent != null) ? parent : GUI.GUIRoot);
			RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
			rectTransform.SetTextAnchor(anchor);
			rectTransform.anchoredPosition = offset;
			Text text2 = gameObject.AddComponent<Text>();
			text2.horizontalOverflow = HorizontalWrapMode.Overflow;
			text2.verticalOverflow = VerticalWrapMode.Overflow;
			text2.alignment = anchor;
			text2.text = text;
			text2.font = ResourceManager.LoadAssetBundle("shared_auto_001").LoadAsset<Font>("04B_03__");
			text2.fontSize = font_size;
			text2.color = Color.white;
			return text2;
		}

		public static void UpdateUIOffset()
		{
			var width = Screen.width;
			var height = Screen.height;
			int num = BraveMathCollege.GreatestCommonDivisor(Screen.width, Screen.height);
			int num2 = Screen.width / num;
			int num3 = Screen.height / num;
			bool isBestAspectRatio = num2 == 16 && num3 == 9;
			float a = (float)Screen.width / 480f;
			float b = (float)Screen.height / 270f;
			bool isPixelPerfect = Mathf.Min(a, b) % 1f == 0f;

			var heightOffset = height / 3;
			if (width == 2560 && height == 1440 && isBestAspectRatio && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(100f, heightOffset);
				ETG_Gunnect_Client.FontSize = 42;
			}
			else if (width == 1920 && height == 1440 && !isBestAspectRatio && isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(15f, heightOffset);
				ETG_Gunnect_Client.FontSize = 42;
			}
			else if (width == 1920 && height == 1200 && !isBestAspectRatio && isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(15f, heightOffset);
				ETG_Gunnect_Client.FontSize = 38;
			}
			else if (width == 1920 && height == 1080 && isBestAspectRatio && isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(15f, heightOffset);
				ETG_Gunnect_Client.FontSize = 38;
			}
			else if (width == 1768 && height == 992 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(175f, heightOffset);
				ETG_Gunnect_Client.FontSize = 32;
			}
			else if (width == 1680 && height == 1050 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(130f, heightOffset);
				ETG_Gunnect_Client.FontSize = 32;
			}
			else if (width == 1600 && height == 1200 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(92f, heightOffset);
				ETG_Gunnect_Client.FontSize = 32;
			}
			else if (width == 1600 && height == 1024 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(90f, heightOffset);
				ETG_Gunnect_Client.FontSize = 32;
			}
			else if (width == 1600 && height == 900 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(90f, heightOffset);
				ETG_Gunnect_Client.FontSize = 30;
			}
			else if (width == 1440 && height == 900 && !isBestAspectRatio && isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(10f, heightOffset);
				ETG_Gunnect_Client.FontSize = 28;
			}
			else if (width == 1366 && height == 768 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(210f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1360 && height == 768 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(207f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1280 && height == 1024 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(167f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1280 && height == 960 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(167f, heightOffset);
				ETG_Gunnect_Client.FontSize = 22;
			}
			else if (width == 1280 && height == 800 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(167f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1280 && height == 768 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(167f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1280 && height == 720 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(167f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1176 && height == 664 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(115f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1152 && height == 864 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(103f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 1024 && height == 768 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(40f, heightOffset);
				ETG_Gunnect_Client.FontSize = 20;
			}
			else if (width == 800 && height == 600 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(163f, heightOffset);
				ETG_Gunnect_Client.FontSize = 10;
			}
			else if (width == 720 && height == 576 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(124f, heightOffset);
				ETG_Gunnect_Client.FontSize = 10;
			}
			else if (width == 720 && height == 480 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(124f, heightOffset);
				ETG_Gunnect_Client.FontSize = 10;
			}
			else if (width == 640 && height == 480 && !isPixelPerfect)
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(83f, heightOffset);
				ETG_Gunnect_Client.FontSize = 10;
			}
			else
			{
				ETG_Gunnect_Client.PlayerInfoGroupRectTransform.anchoredPosition = new Vector2(100f, heightOffset);
				ETG_Gunnect_Client.FontSize = 42;
			}
		}


		public static void SetTextAnchor(this RectTransform r, TextAnchor anchor)
		{
			r.anchorMin = GUI.AnchorMap[anchor];
			r.anchorMax = GUI.AnchorMap[anchor];
			r.pivot = GUI.AnchorMap[anchor];
		}

		public static Font font;

		public static Transform GUIRoot;

		public static Transform GUIController;

		public static Canvas m_canvas;

		public static readonly Dictionary<TextAnchor, Vector2> AnchorMap = new Dictionary<TextAnchor, Vector2>
		{
			{
				TextAnchor.LowerLeft,
				new Vector2(0f, 0f)
			},
			{
				TextAnchor.LowerCenter,
				new Vector2(0.5f, 0f)
			},
			{
				TextAnchor.LowerRight,
				new Vector2(1f, 0f)
			},
			{
				TextAnchor.MiddleLeft,
				new Vector2(0f, 0.5f)
			},
			{
				TextAnchor.MiddleCenter,
				new Vector2(0.5f, 0.5f)
			},
			{
				TextAnchor.MiddleRight,
				new Vector2(1f, 0.5f)
			},
			{
				TextAnchor.UpperLeft,
				new Vector2(0f, 1f)
			},
			{
				TextAnchor.UpperCenter,
				new Vector2(0.5f, 1f)
			},
			{
				TextAnchor.UpperRight,
				new Vector2(1f, 1f)
			}
		};

		private static Color defaultTextColor = new Color(1f, 1f, 1f, 0.5f);
	}
}
