﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Net;
using System.Net.Sockets;

namespace ETG_Gunnect_Client
{
    class GunnectClient : MonoBehaviour
    {
        public const int PORT = 26951;

        public static GunnectClient Instance;
        public static int DataBufferSize = 4096;

        public string IP;
        public string PreviouslyConnectedIP;
        public string Username;
        public int Port = PORT;
        public int Id = 0;
        public TCP Tcp;

        private static Dictionary<int, PacketHandler> _packetHandlers;
        private delegate void PacketHandler(Packet packet);

        public bool IsConnected = false;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Console.WriteLine("ETG_Gunnect_Client - Instance already exists, destroying object!");
                Destroy(this);
            }
        }

        private void Start()
        {
            Tcp = new TCP();
        }

        private void OnApplicationQuit()
        {
            Disconnect();
        }

        public void ConnectToServer(string serverIP, string username)
        {
            IP = serverIP;
            Username = username;
            InitializeClientData();

            IsConnected = true;
            Tcp.Connect();
        }

        public class TCP
        {
            public TcpClient Socket;

            private NetworkStream _stream;
            private Packet _receivedData;
            private byte[] _receiveBuffer;

            public void Connect()
            {
                try
                {
                    Socket = new TcpClient
                    {
                        ReceiveBufferSize = DataBufferSize,
                        SendBufferSize = DataBufferSize
                    };

                    _receiveBuffer = new byte[DataBufferSize];
                    Socket.BeginConnect(Instance.IP, Instance.Port, ConnectCallback, Socket);
                }
                catch (Exception)
                {
                    ETGModConsole.Log("Failed to connect to server!");
                    GunnectClient.Instance.Disconnect();
                }
            }

            private void ConnectCallback(IAsyncResult result)
            {
                Socket.EndConnect(result);

                if (!Socket.Connected)
                {
                    return;
                }

                _stream = Socket.GetStream();

                _receivedData = new Packet();

                _stream.BeginRead(_receiveBuffer, 0, DataBufferSize, ReceiveCallback, null);
            }

            public void SendData(Packet packet)
            {
                try
                {
                    if (Socket != null)
                    {
                        _stream.BeginWrite(packet.ToArray(), 0, packet.Length(), null, null);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error sending data to server via TCP: {e}");
                }
            }

            private void ReceiveCallback(IAsyncResult result)
            {
                try
                {
                    int byteLength = _stream.EndRead(result);

                    if (byteLength <= 0)
                    {
                        Instance.Disconnect();
                        return;
                    }

                    byte[] data = new byte[byteLength];
                    Array.Copy(_receiveBuffer, data, byteLength);

                    _receivedData.Reset(HandleData(data));
                    _stream.BeginRead(_receiveBuffer, 0, DataBufferSize, ReceiveCallback, null);
                }
                catch (Exception e)
                {
                    Disconnect();
                    Console.WriteLine($"Error during ReceiveCallback: {e.Message}");
                }
            }

            private bool HandleData(byte[] data)
            {
                int packetLength = 0;

                _receivedData.SetBytes(data);

                if (_receivedData.UnreadLength() >= 4)
                {
                    packetLength = _receivedData.ReadInt();
                    if (packetLength <= 0)
                    {
                        return true;
                    }
                }

                while (packetLength > 0 && packetLength <= _receivedData.UnreadLength())
                {
                    byte[] packetBytes = _receivedData.ReadBytes(packetLength);
                    ThreadManager.ExecuteOnMainThread(() =>
                    {
                        using (Packet packet = new Packet(packetBytes))
                        {
                            int packetId = packet.ReadInt();
                            _packetHandlers[packetId](packet);
                        }
                    });

                    packetLength = 0;
                    if (_receivedData.UnreadLength() >= 4)
                    {
                        packetLength = _receivedData.ReadInt();
                        if (packetLength <= 0)
                        {
                            return true;
                        }
                    }
                }

                if (packetLength <= 1)
                {
                    return true;
                }

                return false;
            }

            private void Disconnect()
            {
                Instance.Disconnect();

                _stream = null;
                _receivedData = null;
                _receiveBuffer = null;
                Socket = null;
            }
        }

        private void InitializeClientData()
        {
            _packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)ServerPackets.Welcome, GunnectClientHandle.Welcome },
                { (int)ServerPackets.DynamicServerMessage, GunnectClientHandle.DynamicServerMessage },
                { (int)ServerPackets.UpdateClientModDataMessage, GunnectClientHandle.UpdateClientModDataMessage },
            };
        }

        public void Disconnect()
        {
            if (IsConnected)
            {
                PreviouslyConnectedIP = IP;
                IsConnected = false;
                Tcp.Socket.Close();

                ETGModConsole.Log("Disconnected from server.");

                foreach (Transform child in ETG_Gunnect_Client.PlayerInfoGroup.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                var textObject = GUI.CreateText(null, new Vector2(0f, 0f), "Offline", TextAnchor.MiddleLeft, ETG_Gunnect_Client.FontSize);
                textObject.text = "Offline";
                textObject.transform.parent = ETG_Gunnect_Client.PlayerInfoGroup.transform;
            }
        }
    }
}
