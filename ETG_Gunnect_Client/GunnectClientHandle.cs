﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ETG_Gunnect_Client
{
    class GunnectClientHandle : MonoBehaviour
    {
        public static void Welcome(Packet packet)
        {
            string msg = packet.ReadString();
            int id = packet.ReadInt();

            ETGModConsole.Log($"<color=#FFD700>{msg}</color>");
            GunnectClient.Instance.Id = id;
            if (String.IsNullOrEmpty(GunnectClient.Instance.Username))
            {
                GunnectClient.Instance.Username = $"Player {id}";
            }
            ClientSend.WelcomeReceived();
        }


        public static void DynamicServerMessage(Packet packet)
        {
            int fromClientId = packet.ReadInt();
            string modId = packet.ReadString();
            string dataTypeName = packet.ReadString();
            string data = packet.ReadString();

            string fromClientUsername = ETG_Gunnect_Client.PlayerInfos.SingleOrDefault(x => x.Id == fromClientId).Username;

            var invokeMethod = GetModMethodInfo(modId, "DynamicServerMessageHandler");
            invokeMethod.Invoke(null, new object[] { fromClientId, fromClientUsername, dataTypeName, data });
        }

        public static void UpdateClientModDataMessage(Packet packet)
        {
            string modId = packet.ReadString();
            string serializedPlayerInfos = packet.ReadString();
            string serializedClientModData = packet.ReadString();

            ETG_Gunnect_Client.PlayerInfos = JsonConvert.DeserializeObject<List<PlayerInfo>>(serializedPlayerInfos);
            var clientModData = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<string, ModData>>>(serializedClientModData);

            // Update UI
            UpdatePlayerListUI(clientModData);
            LogPlayerList();

            var filteredClientData = new Dictionary<int, string>();
            // Filter dict by mod id
            if (modId != "Gunnect+ Client")
            {
                foreach (var clientId in clientModData.Keys)
                {
                    if (clientModData[clientId].ContainsKey(modId))
                    {
                        filteredClientData[clientId] = clientModData[clientId][modId].SerializedData;
                    }
                }
            }

            // Return filtered client mod data
            var invokeMethod = GetModMethodInfo(modId, "UpdateClientModDataMessageHandler");
            invokeMethod.Invoke(null, new object[] { filteredClientData });
        }

        private static void UpdatePlayerListUI(Dictionary<int, Dictionary<string, ModData>> clientModData)
        {
            foreach (Transform child in ETG_Gunnect_Client.PlayerInfoGroup.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            foreach (var playerInfo in ETG_Gunnect_Client.PlayerInfos)
            {
                if (clientModData[playerInfo.Id] != null)
                {
                    var playerTextObject = GUI.CreateText(null, new Vector2(0f, 0f), playerInfo.Username, TextAnchor.MiddleLeft, ETG_Gunnect_Client.FontSize);
                    playerTextObject.text = playerInfo.Username;
                    playerTextObject.transform.parent = ETG_Gunnect_Client.PlayerInfoGroup.transform;

                    foreach (var modId in clientModData[playerInfo.Id].Keys)
                    {
                        var uiEntries = clientModData[playerInfo.Id][modId].UIEntries;
                        if (uiEntries != null)
                        {
                            foreach (var uiEntry in uiEntries)
                            {
                                var dataTextObject = GUI.CreateText(null, new Vector2(0f, 0f), $@"  - {uiEntry}", TextAnchor.MiddleLeft, ETG_Gunnect_Client.FontSize);
                                dataTextObject.text = $@"  - {uiEntry}";
                                dataTextObject.transform.parent = ETG_Gunnect_Client.PlayerInfoGroup.transform;
                            }
                        }
                    }
                }
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(ETG_Gunnect_Client.PlayerInfoGroup.transform as RectTransform);
        }

        public static void LogPlayerList()
        {
            ETGModConsole.Log("");
            ETGModConsole.Log("Player List");
            ETGModConsole.Log("-----------");
            foreach (var playerInfo in ETG_Gunnect_Client.PlayerInfos)
            {
                ETGModConsole.Log($"{playerInfo.Username}");
            }
            ETGModConsole.Log("");
        }

        private static MethodInfo GetModMethodInfo(string modId, string methodName)
        {
            var mod = ETGMod.AllMods.SingleOrDefault(x => x.GetType().Name == modId);
            return mod.GetType().GetMethod(methodName);
        }
    }
}
