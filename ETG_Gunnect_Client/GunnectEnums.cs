﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETG_Gunnect_Client
{
    public enum SendType
    {
        All = 0,
        AllExcludingSender = 1,
        FromList = 2
    }
}
